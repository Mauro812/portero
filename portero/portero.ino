/*
192.168.4.1/conexion
*/

#include <WiFi.h>
#include <EEPROM.h>
#include <FastLED.h>
#include <ESPAsyncWebServer.h>
#include <HTTPClient.h>

const char HTML[] PROGMEM = "<!DOCTYPE html><html lang=\"en\"><head> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script> <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> <title>Portero Connection</title> <style type=\"text/css\"> *{margin: 0; padding: 0; font-family: sans-serif; box-sizing: border-box;}body{background: #002655; display: flex; min-height: 100vh; padding-bottom: 200px;} form {margin: auto; border: 1px solid rgba(0,0,0,0.2); width: 50%; max-width: 500px; background: #FFF; padding: 30px;} h2{text-align: center; font-size:30px; margin: 20px; color: #002655;} input{display: block; padding: 10px; width: 100%; margin: 30px 0; font-size: 20px;} input[type='submit']{background: linear-gradient(#002655, #0062cc); border: 0; color: white; opacity: 0.8; cursor: pointer; border-radius: 20px; margin-bottom: 0;} input[type='submit']:hover{ opacity: 1;} input[type='submit']:active{transform: scale(0.95);} @media (max-width:768px){form{width: 75%;}} @media (max-width:768px){form{width: 75%;}}</style> </head> <script type=\"text/javascript\">function recuperaDato(){var pass=document.getElementById(\"contra\").value; var ssid=document.getElementById(\"nombre\").value;}</script><body> <body> </div><form action=\"/conexion\" method=\"GET\" onsubmit=\"recuperaDato()\"> <h2> RED PORTERO </h2><input type=\"text\" placeholder='Red' name=\"ssid\" id=\"nombre\"><input type='password' placeholder='Contrase&ntilde;a' name=\"pass\" id=\"contra\"><input type='submit' name=\"boton\" value='Conectar'><h1 style='font-size:10px; text-align: center; padding-top:30px;'>* Asegurese de conectarse a una red wifi de 2.4Ghz </h1></form></body></html>";

HTTPClient http; // Se  crea una variable para poder comunicarse con la API.
HardwareSerial mySerial(2); // Permite comunicacion serial desde otro puerto del ESP32.
AsyncWebServer server(80); // Puerto en el que el servidor sera escuchado.

int freq = 2000;
int channel = 0;
int resolution = 8;

String codigoQR; // Se almacena el codigo QR leido.
String Red = ""; // Se almacena el nombre de la red ingresado por el usuario
String Contrasena = ""; // Se almacena la contrasena de la Red.

char s[50]; // Almacena la red para ser ingresada en la funcion WIFI.begin().
char p[50]; // Almacena la contrasena para ser ingresada en la funcion WIFI.begin().

const char *ssid = "Portero"; // Nombre de la Red del ESP32
const int Boton = 5; // Boton cual actividad es activar el modo modem en el ESP32.
const int LedTrue = 19; // Led que inidca que el codigo QR es correcto.
const int DATA_PIN = 22; // Led que indica que el codigo QR es incorrecto.

bool mode_lector = true; // Variable que si es verdadera indica que el ESP32 esta configurado para leer codigos QR.

#define NUM_LEDS 16
CRGB leds[NUM_LEDS];

void setup() {
  Serial.begin(115200); // Se inicializa comunicacion Serial.
  mySerial.begin(115200, SERIAL_8N1 , 16, 17); // Se inicializa comunicacion serial a traves de los pines 16 y 17.
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(21, channel);
  //ESP.restart();
  EEPROM.begin(512); // Inicializa la comunicacion con memoria flash.
  server_init(true); // Configura el modo servidor.
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  pinMode(Boton,INPUT_PULLDOWN); // Se pone el pin seleccionado en modo entrada
  pinMode(LedTrue,OUTPUT); // Se pone el pin seleccionado en modo salida 
  Red = leer(0); // Se lee la memoria flash a partir de la posicion 0.
  Contrasena = leer(50); // Se lee la memoria flash a partir de la posicion 50.
  if(Red == "" || Contrasena == "") // Condicion que valida si se encontro informacion en memoria flash.
    mode_lector = false;
}

void loop() {
  /* Si los datos fueron ingresados correctamente el dispositivo se conecta a la red.*/
  if(!mode_lector && Red != "" && Contrasena != "") 
    conexion_internet();
  /* Si el dispositivo no esta conectado a la red el led de conexion se mantiene paroadeando.*/  
  if(WiFi.status()!= WL_CONNECTED && !mode_lector){ // 
    color_leds(1);
    FastLED.show();
    delay(200);
    color_leds(5);
    FastLED.show();
    delay(200);
  }
  /*Si el dispositivo esta conectado a la red se configuara para leer codigos QR*/
  if(WiFi.status()== WL_CONNECTED && mode_lector){
    while(mode_lector){ // Mientras el boton no sea presionado el dispositivo se mantiene leyendo codigos QR.
        color_leds(2);
        FastLED.show();
        if(digitalRead(Boton) == LOW) // Si el boton es presionado se activa el modo modem.
            server_init(false);
         if(mySerial.available()) { // Si un codigo QR es leido ingresa a este proceso.
            codigoQR = mySerial.readStringUntil('\n'); // Se le la informacion de el puerto serial sin saltos de linea.
            codigoQR = codigoQR.substring(0,codigoQR.length()-1); // Se almacena la informacion en una variable y se le vuelve a quitar los saltos de linea.
            Serial.println(codigoQR);
            codigoQR = codigoQR;
            String cadena = "{\"idInvitacion\":\""+codigoQR+"\"}"; // Se concatena el codigo leido al json que sera enviado a la API.
            Serial.println(cadena); 
            int httpCode = http.POST(cadena);  // Se envia el codigo QR a la API.
            if(httpCode>0){ // si la api regreso una respuesta
              String response = http.getString(); // Se lee la respuesta de la API.
              Serial.println("La respuesta es: " + response);
              if(response == "true"){ // Si la respuesta es valida se prende el led de verdadero
                digitalWrite(LedTrue, HIGH);
                //delay(200);
                color_leds(3);
                FastLED.show();
                delay(100);
                acierto();
                delay(1000);
                color_leds(5);
                FastLED.show();
                delay(100);
                digitalWrite(LedTrue, LOW);
              }
              if(response != "true"){ // Si la respuesta no es valida se prende el led de falso.
                color_leds(4);
                FastLED.show();
                delay(100);
                error();
                delay(500);
                color_leds(5);
                FastLED.show();
              }
            }
            else{ // Si la Api no regreso respuesta.
              color_leds(4);
              FastLED.show();
              delay(100);
              error();
              delay(500);
              color_leds(5);
              FastLED.show();
              }
          }
    }
  }
}
/* Funcion que hace posible que el dispositivo se conecte a la red*/
void conexion_internet()
{
    Serial.println("La red es =" + Red);
    Serial.println("La contrasena es =" + Contrasena );
    strcpy(p,Contrasena.c_str());
    strcpy(s,Red.c_str());
    WiFi.mode(WIFI_OFF);
    WiFi.begin(s,p); // Se inicializa la coneccion WIFI.
    long tiempoA = millis(); // Se inicializa un timer.
    while(millis()-tiempoA<10000 && WiFi.status() != WL_CONNECTED) // Se valida que el tiempo de conexion no haya caducado.
    {
          delay(1000);
          Serial.println("Conectando a la red ..");
          if(digitalRead(Boton) == LOW){ // Si el boton es presionado se activa el modo modem.
            Serial.println("Boton presionado, modo modem Activado");
            ESP.restart();
            delay(1000);
            //server_init(false);
            //break;
          }     
    }
    if(WiFi.status()== WL_CONNECTED){ // Si el dispositivo es conectado correctamente a la red.
       color_leds(2);
       FastLED.show();
       grabar(0,Red); // Se graba el nombre de la red en la memoria flash.
       grabar(50,Contrasena); // Se graba la contrasena en memoria flash.
       mode_lector = true; // Se activa la bandera mode_lector para indicar que el dispositivo esta listo para leer codigos QR.
       http.begin("https://porteroslp.herokuapp.com/api/portero/");  // Se inicia la conexion con la API notificaciones
       http.addHeader("Content-Type", "application/json"); //Specify content-type header  // Especifica el contenido en la cabecera
    }
}
void server_init(bool init)
{
    WiFi.mode(WIFI_OFF); // Se desactiva la conexion WIFI.
    WiFi.softAP(ssid);  // Se configura el nombre de la red para iniciar el modo modem.
    Serial.print("IP address: ");
    Serial.println(WiFi.softAPIP()); // Se imprime la direcion IP de el servidor (ESP32).
    /*Aqui se activa la pagina web y espera que los datos sean ingresados por los usuarios*/
      server.on("/conexion", HTTP_GET, [](AsyncWebServerRequest *request){ 
      int paramsNr = request->params(); 
      for(int i=0;i<paramsNr;i++){      
          AsyncWebParameter* p = request->getParam(i);
          if (i == 0)
            Red = p->value();
          if (i == 1)
            Contrasena = p->value();     
      }
      request->send(200, "text/html", HTML);
      });
      server.begin(); // Inicializa el modo servidor.
    if(!init){ // Se borran los datos de memoria flash.
      Red = ""; // 
      Contrasena = "";
      grabar(0,Red);
      grabar(50,Contrasena);}
    mode_lector = false; // Se desactiva la bandera modem_true.
}
 /*Funcion que escribe informacion en la memoria flash*/
void grabar(int addr, String a) {
  int tamano = a.length(); 
  char inchar[50]; 
  a.toCharArray(inchar, tamano+1);
  for (int i = 0; i < tamano; i++) {
    EEPROM.write(addr+i, inchar[i]);
  }
  for (int i = tamano; i < 50; i++) {
    EEPROM.write(addr+i, 255);
  }
  EEPROM.commit();
}
/*Funcion que lee funcion de la memoria flash*/
String leer(int addr) {
   byte lectura;
   String strlectura;
   for (int i = addr; i < addr+50; i++) {
      lectura = EEPROM.read(i);
      if (lectura != 255) {
        strlectura += (char)lectura;
      }
   }
   return strlectura;
}

void error()
{
  ledcWriteTone(channel, 1755);
  delay(500);
  ledcWriteTone(channel, 3255);
  delay(500);
  ledcWriteTone(channel, 0);
  delay(100);
}

void acierto()
{
  ledcWriteTone(channel, 1755);
  delay(100);
  ledcWriteTone(channel, 0);
  delay(100);
  ledcWriteTone(channel, 1755);
  delay(100);
  ledcWriteTone(channel, 0);
  delay(100);
}

void color_leds(int color)
{
  switch(color){
    case 1:
          for(int i=0;i<NUM_LEDS;i++)
            leds[i] = CRGB::Orange;
          break;
    case 2:
          for(int i=0;i<NUM_LEDS;i++)
            leds[i] = CRGB::Blue;
          break;
    case 3:
          for(int i=0;i<NUM_LEDS;i++)
            leds[i] = CRGB::Green;
          break;
    case 4:
          for(int i=0;i<NUM_LEDS;i++)
            leds[i] = CRGB::Red;
          break;
    case 5:
          for(int i=0;i<NUM_LEDS;i++)
            leds[i] = CRGB::Black;
          break;
    }
}
